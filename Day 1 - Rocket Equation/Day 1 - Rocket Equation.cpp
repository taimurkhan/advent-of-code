#include <iostream>
#include <fstream>

using namespace std;

int fuelCalculator(int moduleMass)
{
    int fuel = (moduleMass / 3) - 2;
    while (fuel > 2)
    {
        return fuel += fuelCalculator(fuel);
    }
    return fuel;
}

int main()
{
    int totalFuel = 0, mass = 0;

    ifstream inputfile("values.txt");
    while (inputfile >> mass)
    {
        totalFuel += fuelCalculator(mass);
    }

    cout << "\nTotal fuel cost is: " << totalFuel << endl;

    return 0;
}